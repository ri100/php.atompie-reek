<?php
$root =  getcwd() . '/..';
$buildRoot =  getcwd() ;

if(file_exists('reek.phar')) {
	unlink('reek.phar');
}

$phar = new Phar(
	$buildRoot . "/reek.phar",
	FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::KEY_AS_FILENAME,
	"reek.phar"
);
$phar->buildFromDirectory($root, '/^((?!\.idea).)*$/');
$phar->setStub($phar->createDefaultStub("src/AtomPie/Cli/Reek/reek.php"));