<?php
namespace AtomPie\Cli\Reek\Domain {

    class JobParamSet
    {
        private $sSource;
        private $aAllow;
        private $aDeny;
        private $aSkip;
        private $iRecursionDepth;
        private $bVerbose;
        private $bWithNamespace;
        private $bVersion;
        private $bFlipReporting;
        private $bJsonReporting;
        private $sDescription;

        public function __construct(
            $sSource,
            $aAllow,
            $aDeny,
            $aSkip,
            $iRecursionDepth,
            $bVerbose,
            $bWithNamespace,
            $bVersion,
            $bFlipReporting,
            $bJsonReporting,
            $sDescription
        ) {
            $this->sSource = $sSource;
            $this->aAllow = $aAllow;
            $this->aDeny = $aDeny;
            $this->aSkip = $aSkip;
            $this->iRecursionDepth = $iRecursionDepth;
            $this->bVerbose = $bVerbose;
            $this->bWithNamespace = $bWithNamespace;
            $this->bVersion = $bVersion;
            $this->bFlipReporting = $bFlipReporting;
            $this->bJsonReporting = $bJsonReporting;
            $this->sDescription = $sDescription;
        }

        /**
         * @return string
         */
        public function getSource()
        {
            return $this->sSource;
        }

        /**
         * @return array
         */
        public function getAllow()
        {
            return $this->aAllow;
        }

        /**
         * @return array
         */
        public function getDeny()
        {
            return $this->aDeny;
        }

        /**
         * @return array
         */
        public function getSkip()
        {
            return $this->aSkip;
        }

        /**
         * @return int
         */
        public function getRecursionDepth()
        {
            return $this->iRecursionDepth;
        }

        /**
         * @return bool
         */
        public function getVerbose()
        {
            return $this->bVerbose;
        }

        /**
         * @return bool
         */
        public function getWithNamespace()
        {
            return $this->bWithNamespace;
        }

        /**
         * @return bool
         */
        public function getVersion()
        {
            return $this->bVersion;
        }

        /**
         * @return bool
         */
        public function getFlipReporting()
        {
            return $this->bFlipReporting;
        }

        /**
         * @return bool
         */
        public function getJsonReporting()
        {
            return $this->bJsonReporting;
        }

        /**
         * @return string
         */
        public function getDescription()
        {
            return $this->sDescription;
        }
    }

}
