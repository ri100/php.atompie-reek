<?php
namespace AtomPie\Cli\Reek\Domain {

    class ReekReport implements \JsonSerializable
    {

        private $aFoundFiles;
        private $aSkippedFiles;
        private $aConflictsReportByFileName;

        public function __construct($aFoundFiles, $aSkippedFiles, $aConflictsReportByFileName)
        {
            $this->aFoundFiles = $aFoundFiles;
            $this->aSkippedFiles = $aSkippedFiles;
            $this->aConflictsReportByFileName = $aConflictsReportByFileName;
        }

        /**
         * @return array
         */
        public function getFoundFiles()
        {
            return $this->aFoundFiles;
        }

        /**
         * @return array
         */
        public function getSkippedFiles()
        {
            return $this->aSkippedFiles;
        }

        /**
         * @return ReekSpot[]
         */
        public function getConflictsReportByFileName()
        {
            return $this->aConflictsReportByFileName;
        }

        /**
         * @return bool
         */
        public function hasConflicts()
        {
            return !empty($this->aConflictsReportByFileName);
        }

        /**
         * Specify data which should be serialized to JSON
         * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
         * @return mixed data which can be serialized by <b>json_encode</b>,
         * which is a value of any type other than a resource.
         * @since 5.4.0
         */
        function jsonSerialize()
        {
            return array(
                'Found' => $this->getFoundFiles(),
                'Skipped' => $this->getSkippedFiles(),
                'Conflicts' => $this->getConflictsReportByFileName()
            );
        }
    }

}
