<?php
namespace AtomPie\Cli\Reek\Domain {

    class ReekSpot
    {

        private $aReeks;
        private $sCode;

        public function __construct($aReeks, $sCode)
        {

            $this->aReeks = $aReeks;
            $this->sCode = $sCode;

        }

        /**
         * @return bool
         */
        public function isReeking()
        {
            return !empty($this->aReeks);
        }

        /**
         * @return array
         */
        public function getReeks()
        {
            return $this->aReeks;
        }

        /**
         * @return array
         */
        public function getCode()
        {
            return $this->sCode;
        }
    }

}
