<?php
namespace AtomPie\Cli\Reek\Infrastructure {

    use Symfony\Component\Yaml\Yaml;

    class YamlConfig
    {

        private $sFilePath;

        public function __construct($sFilePath)
        {
            $this->sFilePath = $sFilePath;
        }

        /**
         * @param $sSource
         * @param $aJobConfig
         * @return array
         */
        public function rewriteYamlConfig($sSource, $aJobConfig)
        {
            $aConfig = array();

            $aConfig['source'] = $sSource;
            if (isset($aJobConfig['Deny'])) {
                $aConfig['deny'] = $aJobConfig['Deny'];
            }
            if (isset($aJobConfig['Allow'])) {
                $aConfig['allow'] = $aJobConfig['Allow'];
            }
            if (isset($aJobConfig['Skip'])) {
                $aConfig['skip'] = $aJobConfig['Skip'];
            }
            if (isset($aJobConfig['RecursionDepth'])) {
                $aConfig['recursion-depth'] = $aJobConfig['RecursionDepth'];
            }
            if (isset($aJobConfig['Verbose']) and strtolower($aJobConfig['Verbose']) == 'yes') {
                $aConfig['verbose'] = true;
            }
            if (isset($aJobConfig['WithNamespaces']) and strtolower($aJobConfig['WithNamespaces']) == 'yes') {
                $aConfig['with-namespaces'] = true;
            }
            if (isset($aJobConfig['FlipReport']) and strtolower($aJobConfig['FlipReport']) == 'yes') {
                $aConfig['flip-report'] = true;
            }
            if (isset($aJobConfig['JsonReport']) and strtolower($aJobConfig['JsonReport']) == 'yes') {
                $aConfig['json-report'] = true;
            }
            if (isset($aJobConfig['Description'])) {
                $aConfig['description'] = $aJobConfig['Description'];
            }

            return $aConfig;
        }

        /**
         * @return mixed
         * @throws \Exception
         */
        public function parseYaml()
        {
            $sConfigPath = $this->sFilePath;
            if (!is_file($sConfigPath)) {
                throw new \Exception(sprintf('Invalid config path %s.', $sConfigPath));
            }

            return Yaml::parse(file_get_contents($sConfigPath));
        }

    }

}
