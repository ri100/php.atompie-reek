<?php
namespace AtomPie\Cli\Reek\Service {

    use AtomPie\Cli\Reek\Domain\JobParamSet;
    use Ulrichsg\Getopt\Getopt;
    use Ulrichsg\Getopt\Option;

    class CliParams
    {

        const VERSION = "0.2.0";

        public function hasConfig($oOptions)
        {
            return isset($oOptions['config']);
        }

        /**
         * @return Getopt
         */
        public function getParamDefinitions()
        {
            $confOption = new Option(null, 'config', Getopt::REQUIRED_ARGUMENT);
            $confOption->setDescription('YAML configuration.');
            $versionOption = new Option(null, 'version');
            $versionOption->setDescription('Version of reek.');
            $allowOption = new Option('a', 'allow', Getopt::REQUIRED_ARGUMENT);
            $allowOption->setDescription('Comma separated values of allowed namespaces.');
            $denyOption = new Option('d', 'deny', Getopt::REQUIRED_ARGUMENT);
            $denyOption->setDescription('Comma separated values of denied namespaces.');
            $folderOption = new Option('s', 'source', Getopt::REQUIRED_ARGUMENT);
            $folderOption->setDescription('Namespace to check. Code source to check. It can be file or directory.');
            $skipOption = new Option(null, 'skip', Getopt::REQUIRED_ARGUMENT);
            $skipOption->setDescription('Skip namespaces.');

            $recursionDepthOption = new Option('r', 'recursion-depth', Getopt::REQUIRED_ARGUMENT);
            $recursionDepthOption->setDescription('Sets recursion depth.');
            $showNamespacesOption = new Option(null, 'with-namespaces');
            $showNamespacesOption->setDescription('Show namespace reference.');
            $verboseOption = new Option(null, 'verbose');
            $verboseOption->setDescription('Show more information.');
            $flipReportOption = new Option(null, 'flip-report');
            $flipReportOption->setDescription('Flip reporting.');

            return new Getopt(array(
                $confOption,
                $allowOption,
                $denyOption,
                $folderOption,
                $skipOption,
                $recursionDepthOption,
                $showNamespacesOption,
                $verboseOption,
                $versionOption,
                $flipReportOption
            ));

        }

        public function getConfig($oOptions)
        {
            return $oOptions['config'];
        }

        /**
         * @param $aOptions
         * @return JobParamSet
         */
        public function getParamsFromArray($aOptions)
        {

            if (!isset($aOptions['source'])) {
                throw new \UnexpectedValueException("Parameter [source] not set. \n\n");
            } else {
                $sSource = $aOptions['source'];
            }

            if (!isset($aOptions['description'])) {
                $sDescription = null;
            } else {
                $sDescription = $aOptions['description'];
            }

            if (isset($aOptions['allow'])) {
                if (is_array($aOptions['allow'])) {
                    $aAllow = $aOptions['allow'];
                } else {
                    $aAllow = explode(',', $aOptions['allow']);
                }
            } else {
                $aAllow = array();
            }

            if (isset($aOptions['deny'])) {
                if (is_array($aOptions['deny'])) {
                    $aDeny = $aOptions['deny'];
                } else {
                    $aDeny = explode(',', $aOptions['deny']);
                }
            } else {
                $aDeny = array();
            }

            if (isset($aOptions['skip'])) {
                if (is_array($aOptions['skip'])) {
                    $aSkip = $aOptions['skip'];
                } else {
                    $aSkip = explode(',', $aOptions['skip']);
                }

            } else {
                $aSkip = array();
            }

            if (isset($aOptions['recursion-depth']) && is_numeric($aOptions['recursion-depth'])) {
                $iRecursionDepth = $aOptions['recursion-depth'];
            } else {
                $iRecursionDepth = 100;
            }

            $bVerbose = isset($aOptions['verbose']);
            $bVersion = isset($aOptions['version']);
            $bWithNamespace = isset($aOptions['with-namespaces']);
            $bFlipReporting = isset($aOptions['flip-report']);
            $bJsonReporting = isset($aOptions['json-report']);

            return new JobParamSet(
                $sSource,
                $aAllow,
                $aDeny,
                $aSkip,
                $iRecursionDepth,
                $bVerbose,
                $bWithNamespace,
                $bVersion,
                $bFlipReporting,
                $bJsonReporting,
                $sDescription
            );
        }

    }

}
