<?php
namespace AtomPie\Cli\Reek\Service {

    use PhpParser\Node;
    use PhpParser\Node\Stmt;
    use PhpParser\NodeVisitorAbstract;

    class NamespaceChecker extends NodeVisitorAbstract
    {

        private $aAllowed;
        private $aDenied;
        private $aConflicts = array();
        private $aAllClasses = array();
        private $aFunctions = array();
        private $aConstants = array();
        private $name;
        private $type = 'unknown';
        private $sCode;

        public function __construct(array $aAllowed, array $aDenied, $sCode)
        {
            $this->aAllowed = $aAllowed;
            $this->aDenied = $aDenied;
            $this->sCode = $sCode;
        }

        public function enterNode(Node $node)
        {
            if ($node instanceof Node\Expr\New_) {
                if (isset($node->class)) {
                    $this->name = $node->class;
                    $this->type = 'references';
                }
            } else {
                if ($node instanceof Stmt\Class_) {
                    if (isset($node->namespacedName)) {
                        $this->name = $node->namespacedName->__toString();
                        $this->type = 'references';
                    }
                } else {
                    if ($node instanceof Stmt\Interface_) {
                        if (isset($node->namespacedName)) {
                            $this->name = $node->namespacedName->__toString();
                            $this->type = 'references';
                        }
                    } else {
                        if ($node instanceof Stmt\Namespace_) {
                            if (isset($node->name)) {
                                $this->name = $node->name->toString();
                                $this->type = 'namespaces';
                            }
                        }
                    }
                }
            }
        }

        public function leaveNode(Node $node)
        {

            // Wyklucza nazwy które są funkcjami i stałymi
            if ($node instanceof Node\Expr\ConstFetch) {
                $this->aConstants[] = $node->name->__toString();
            } elseif ($node instanceof Node\Expr\FuncCall) {
                if ($node->name instanceof Node\Expr\Variable) {
//					var_dump($node->name);exit;

                } else {
                    if ($node->name instanceof Node\Name) {
                        $this->aFunctions[] = $node->name->__toString();
                    }
                }
            } elseif ($node instanceof Node\Name) {
                $this->AddAndCheck($node, $node->__toString());
            }
        }

        public function afterTraverse(array $nodes)
        {
            if (!empty($this->aAllowed)) {

                foreach ($this->aAllClasses as $sType => $aTypes) {
                    foreach ($aTypes as $sCurrentClassName => $aLines) {

                        // Is function or constant or self, Allow
                        if ($sCurrentClassName == 'self' || in_array($sCurrentClassName,
                                $this->aFunctions) || in_array($sCurrentClassName, $this->aConstants)
                        ) {
                            unset($this->aAllClasses[$sType][$sCurrentClassName]);
                        }

                        // Allow
                        foreach ($this->aAllowed as $sAllowedClassName) {
                            $sAllowedClassName = str_replace('.', '\\', $sAllowedClassName);
                            if ($this->startsWith($sCurrentClassName, $sAllowedClassName)) {
                                unset($this->aAllClasses[$sType][$sCurrentClassName]);
                            }
                        }
                    }

                }
                // Merge (add to conflicts)
                foreach ($this->aAllClasses as $sType => $aTypes) {
                    foreach ($aTypes as $sCurrentClassName => $aLines) {
                        /**
                         * @var \PhpParser\Node $oNode
                         */
                        foreach ($aLines as $iLine => $oNode) {
                            $this->aConflicts[$sType][$sCurrentClassName][$iLine] = $oNode;
                        }
                    }
                }

            }
        }

        private function AddAndCheck(Node $node, $sCurrentClassName)
        {
            $iLine = $node->getLine();
            $this->aAllClasses[$this->type][$sCurrentClassName][$iLine] = $node;
            // Deny
            foreach ($this->aDenied as $sDeniedClassName) {
                $sDeniedClassName = str_replace('.', '\\', $sDeniedClassName);
                if ($this->startsWith($sCurrentClassName, $sDeniedClassName)) {
                    $this->aConflicts[$this->type][$sCurrentClassName][$iLine] = $node;
                }
            }
        }

        private function startsWith($aHaystack, $sNeedle)
        {
            // search backwards starting from haystack length characters from the end
            return $sNeedle === "" || strrpos($aHaystack, $sNeedle, -strlen($aHaystack)) !== false;
        }

        public function getConflicts()
        {
            return $this->aConflicts;
        }
    }
}

