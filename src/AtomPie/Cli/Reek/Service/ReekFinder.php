<?php
namespace AtomPie\Cli\Reek\Service {

    use AtomPie\Cli\Reek\Domain\ReekReport;
    use AtomPie\Cli\Reek\Domain\ReekSpot;
    use PhpParser\Error;
    use PhpParser\NodeTraverser;
    use PhpParser\NodeVisitor\NameResolver;
    use PhpParser\Parser;
    use PhpParser\ParserFactory;

    class ReekFinder
    {

        const SKIP_DUE_TO_FILE = 1;

        final public function __construct()
        {
        }

        /**
         * Returns report on conflicting files with given rules.
         *
         * @param $sSource
         * @param $aAllow
         * @param $aDeny
         * @param $aSkip
         * @param $iRecursionDepth
         * @return ReekReport
         * @throws \Exception
         */
        public function find($sSource, $aAllow, $aDeny, $aSkip, $iRecursionDepth = 100)
        {

            $aConflictsReportByFileName = array();
            $aFoundFiles = array();
            $aSkippedFiles = array();

            if (is_file($sSource)) {

                $sFilePath = $sSource;
                $aFoundFiles[$sFilePath] = $sFilePath;

                $sCode = $this->getCode($sFilePath);

                $oReport = $this->getCodeReport($aAllow, $aDeny, $sCode);

                $aConflicts = $oReport->getConflicts();

                if (!empty($aConflicts)) {
                    $aConflictsReportByFileName[$sFilePath] = new ReekSpot($aConflicts, explode(PHP_EOL, $sCode));
                }

                return new ReekReport($aFoundFiles, array(), $aConflictsReportByFileName);


            } elseif (is_dir($sSource)) {

                $sFolder = realpath($sSource);

                $oDirectory = new \RecursiveDirectoryIterator($sFolder, \RecursiveDirectoryIterator::SKIP_DOTS);
                $oIterator = new \RecursiveIteratorIterator($oDirectory, \RecursiveIteratorIterator::SELF_FIRST);

                $oIterator->setMaxDepth($iRecursionDepth);

                foreach ($oIterator as $path) {

                    if (!$path->isDir()) {
                        $sFilePath = $path->__toString();
                        $sFilePath = str_replace('./', '', $sFilePath);

                        if ($this->skip($aSkip, $sFilePath, $sFolder)) {
                            $aSkippedFiles[$sFilePath] = $sFilePath;
                            continue;
                        };

                        $aFoundFiles[$sFilePath] = $sFilePath;

                        $sCode = $this->getCode($sFilePath);
                        $oReport = $this->getCodeReport($aAllow, $aDeny, $sCode);

                        $aConflicts = $oReport->getConflicts();

                        if (!empty($aConflicts)) {
                            $aConflictsReportByFileName[$sFilePath] = new ReekSpot($aConflicts,
                                explode(PHP_EOL, $sCode));
                        }
                    }
                }

                return new ReekReport($aFoundFiles, $aSkippedFiles, $aConflictsReportByFileName);

            } else {
                throw new \UnexpectedValueException(
                    sprintf(
                        "Parameter [source] is not correct. Can not find folder or file %s. \n\n",
                        $sSource
                    )
                );
            }

        }

        private function skip($aSkip, $sFilePath, $sFolder)
        {
            if (!is_array($aSkip)) {
                return false;
            }
            foreach ($aSkip as $sSkippedNamespaceOrClass) {

                $sFilePathWithoutExtension =
                    pathinfo($sFilePath, PATHINFO_DIRNAME) .
                    DIRECTORY_SEPARATOR .
                    pathinfo($sFilePath, PATHINFO_FILENAME);

                // Get namespace from file path

                if (substr($sFilePathWithoutExtension, 0, strlen($sFolder)) == $sFolder) {
                    $sClassNamespaceFromFilePath = substr($sFilePathWithoutExtension, strlen($sFolder) + 1);
                } else {
                    $sClassNamespaceFromFilePath = $sFilePathWithoutExtension;
                }

                // Get namespace separator. Unify separators
                $sClassNamespaceFromFilePath = str_replace('.', '\\', $sClassNamespaceFromFilePath);
                $sClassNamespaceFromFilePath = str_replace(DIRECTORY_SEPARATOR, '\\', $sClassNamespaceFromFilePath);
                $sSkippedNamespaceOrClass = str_replace('.', '\\', $sSkippedNamespaceOrClass);

                if ($this->startsWith($sClassNamespaceFromFilePath, $sSkippedNamespaceOrClass)) {
                    return self::SKIP_DUE_TO_FILE;
                }

            }

            return false;
        }

        private function startsWith($aHaystack, $sNeedle)
        {
            return $sNeedle === "" || strrpos($aHaystack, $sNeedle, -strlen($aHaystack)) !== false;
        }

        /**
         * @param $aAllow
         * @param $aDeny
         * @param $sCode
         * @return NamespaceChecker
         */
        private function getCodeReport($aAllow, $aDeny, $sCode)
        {
            $oParser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
            $oNamespaceChecker = new NamespaceChecker($aAllow, $aDeny, $sCode);
            $oTraverser = new NodeTraverser();
            $oTraverser->addVisitor(new NameResolver());    // we will need resolved names
            $oTraverser->addVisitor($oNamespaceChecker);    // our own node visitor

            try {
                $oTraverser->traverse($oParser->parse($sCode));
            } catch (Error $e) {
                echo 'Parse Error: ', $e->getMessage() . PHP_EOL;
            }

            return $oNamespaceChecker;
        }

        /**
         * @param $sFilePath
         * @return string
         * @throws \Exception
         */
        private function getCode($sFilePath)
        {
            if (!is_file($sFilePath)) {
                throw new \Exception(sprintf('File %s does not exist', $sFilePath));
            }

            $sCode = file_get_contents($sFilePath);
            return $sCode;
        }
    }

}
