<?php
namespace AtomPie\Cli\Reek\View {

    use AtomPie\Cli\Reek\Infrastructure\ColorCLI;
    use AtomPie\Cli\Reek\Domain\ReekSpot;

    class CliReportRenderer
    {

        const SKIP_DUE_TO_FILE = 1;

        public function displayShortReport($aConflictsByAllowDeny)
        {

            echo PHP_EOL . PHP_EOL .
                '--------------------------------------------------------------' . PHP_EOL;
            echo sprintf(' Short report on %s:' . PHP_EOL, date('Y-m-d H:i:s'));
            echo '--------------------------------------------------------------' . PHP_EOL . PHP_EOL;
            echo ' Constrain report' . PHP_EOL;
            if (isset($aConflictsByAllowDeny['Denied']) and is_array($aConflictsByAllowDeny['Denied'])) {
                echo ColorCLI::getColoredString(PHP_EOL . '  Reeking deny constrain:' . PHP_EOL . PHP_EOL, 'red');
                foreach ($aConflictsByAllowDeny['Denied'] as $sKey => $aValue) {
                    $this->displayFound($sKey, $aValue);
                }
                echo PHP_EOL;
            } else {
                echo '  Denied constrain is OK:' . PHP_EOL;
            }


            if (isset($aConflictsByAllowDeny['NotAllowed']) and is_array($aConflictsByAllowDeny['NotAllowed'])) {
                echo ColorCLI::getColoredString(PHP_EOL . '  Reeking not allowed constrain:' . PHP_EOL . PHP_EOL,
                    'red');
                foreach ($aConflictsByAllowDeny['NotAllowed'] as $sKey => $aValue) {
                    $this->displayFound($sKey, $aValue);
                }
                echo PHP_EOL;
            } else {
                echo '  Allowed constrain is OK:' . PHP_EOL;
            }
        }

        public function displayIteratedFiles($aFiles, $aSkippedFiles, $aConflictsReportByFileName)
        {

            echo PHP_EOL . PHP_EOL . '--------------------------------------------------------------' . PHP_EOL;
            echo sprintf(' File report:') . PHP_EOL;
            echo '--------------------------------------------------------------' . PHP_EOL;

            foreach ($aFiles as $sFilePath) {
                if (isset($aSkippedFiles[$sFilePath])) {
                    echo sprintf("\033[33mSkipping file: %s\033[0m" . PHP_EOL, $sFilePath);
                } else {
                    if (isset($aConflictsReportByFileName[$sFilePath])) {
                        echo sprintf("\033[30mResolving file: %s\e[0m - reek found (%d)." . PHP_EOL, $sFilePath,
                            count($aConflictsReportByFileName[$sFilePath]));
                    } else {
                        echo sprintf("\033[30mResolving file: %s\033[0m" . PHP_EOL, $sFilePath);
                    }
                }

            }
        }

        private function processForShortReport($aConflicts)
        {
            $sNotAllowed = array();
            /**
             * @var $oReekSpot ReekSpot
             */
            foreach ($aConflicts as $sFile => $oReekSpot) {

                if (empty($sFile)) {
                    continue;
                }

                if (!$oReekSpot->isReeking()) {
                    continue;
                }
                $aReekSpots = $oReekSpot->getReeks();

                foreach ($aReekSpots as $sType => $aTypes) {
                    foreach ($aTypes as $sClassName => $aLines) {
                        $sNotAllowed[$sClassName][$sType][] = array('File' => $sFile, 'Lines' => $aLines);
                    }
                }
            }

            return $sNotAllowed;
        }

        public function displayConstrainReport($aConflicts)
        {

            echo PHP_EOL . PHP_EOL;
            echo sprintf(' Reek report (date: %s)' . PHP_EOL, date('Y-m-d H:i:s'));
            echo ' --------------------------------------------------------------' . PHP_EOL . PHP_EOL;

            if (empty($aConflicts)) {
                echo "\033[0;32mDone. Everything is fine.\033[0m" . PHP_EOL;
                return;
            }

            echo '  List of constrained classed' . PHP_EOL;
            echo '  ----------------------------' . PHP_EOL . PHP_EOL;
            $i = 0;
            $aNotAllowed = $this->processForShortReport($aConflicts);
            ksort($aNotAllowed);
            foreach ($aNotAllowed as $sClassName => $aData) {
                $i++;
                echo sprintf(" %d. \33[1;36m%s (%s)\33[0m" . PHP_EOL, $i, $sClassName,
                    count(@$aData['references']) + count(@$aData['namespaces']));
                if (isset($aData['references'])) {
                    echo "     \033[34m[reference]\033[0m (" . count($aData['references']) . ")" . PHP_EOL;
                    foreach ($aData['references'] as $aFile) {
                        echo "      - used in: " . $aFile['File'] . ' ';

                        $aLines = array();
                        /** @var $oNode \PhpParser\Node\Name */
                        foreach ($aFile['Lines'] as $oNode) {
                            $aLines[] = $oNode->getLine();
                        }
                        echo " \033[34m(line: " . implode(', ', $aLines) . ")\033[0m";
                        echo PHP_EOL;
                    }
                }
                if (isset($aData['namespaces'])) {
                    echo "     \033[34m[namespace used]\033[0m (" . count($aData['namespaces']) . ")" . PHP_EOL;
                    foreach ($aData['namespaces'] as $aFile) {
                        echo "      - used in: " . $aFile['File'] . ' ';

                        $aLines = array();
                        /** @var $oNode \PhpParser\Node\Name */
                        foreach ($aFile['Lines'] as $oNode) {
                            $aLines[] = $oNode->getLine();
                        }
                        echo " \033[34m(line: " . implode(', ', $aLines) . ")\033[0m";
                        echo PHP_EOL;
                    }
                }

                echo PHP_EOL;
            }

            echo PHP_EOL . "\033[0;31m Total of " . count($aNotAllowed) . " constrained classes reported.\033[0m" . PHP_EOL;

        }

        public function displayFullReport($aConflicts, $bNamespaceShow = false)
        {

            if (empty($aConflicts)) {
                echo "\033[0;32mDone. Everything is fine.\033[0m" . PHP_EOL;
                return;
            }

            echo PHP_EOL . PHP_EOL;
            echo sprintf(' Reek report on %s:' . PHP_EOL, date('Y-m-d H:i:s'));
            echo ' --------------------------------------------------------------' . PHP_EOL;

            echo '  List of files to change' . PHP_EOL;
            echo '  ----------------------------' . PHP_EOL . PHP_EOL;

            /**
             * @var $oReekSpot ReekSpot
             */
            foreach ($aConflicts as $sFile => $oReekSpot) {


                if (!$oReekSpot->isReeking()) {
                    continue;
                }

                $aReekSpots = $oReekSpot->getReeks();

                if (empty($sFile)) {
                    continue;
                }

                echo "\n\033[1;36m" . PHP_EOL;
                echo ' - File to change: ' . $sFile . PHP_EOL;
                echo "\033[0m" . PHP_EOL;

                $CopyOfNamespaces = isset($aReekSpots['namespaces']) ? $aReekSpots['namespaces'] : array();
                $CopyOfReferences = isset($aReekSpots['references']) ? $aReekSpots['references'] : array();

                foreach ($CopyOfNamespaces as $sNamespace => $aLines) {
                    foreach ($CopyOfReferences as $sUsedClassName => $aReferencedLines) {
                        if ($this->startsWith($sUsedClassName, $sNamespace)) {
                            unset($CopyOfNamespaces[$sNamespace]);
                        }
                    }
                }

                if (!empty($CopyOfNamespaces)) {
                    foreach ($CopyOfNamespaces as $sNamespace => $aLines) {
                        echo "\033[33m Warning! Namespace: '$sNamespace' not used or used as PHPDOC extension\033[0m \n";
                    }
                }

                foreach ($aReekSpots as $sType => $aTypes) {
                    if (!$bNamespaceShow) {
                        if ($sType == 'namespaces') {
                            continue;
                        }
                    }
                    echo "\033[34m [" . $sType . "] \033[0m (" . count($aTypes) . ")\n";
                    foreach ($aTypes as $sClassName => $aLines) {
                        if ($sType == 'namespaces') {
                            $sKeyWord = 'use';
                        } else {
                            $sKeyWord = 'depends on';
                        }
                        echo "    " . $sKeyWord . " " . $sClassName;
                        echo "   \033[34m(line: " . implode(',', array_keys($aLines)) . ")\033[0m" . PHP_EOL;
                    }
                }
            }

            echo PHP_EOL . "\033[0;31m Total of " . count($aConflicts) . " incorrect files reported.\033[0m" . PHP_EOL;

        }

        private function displayFound($sKey, $aValue)
        {
            echo ColorCLI::getColoredString(sprintf('  - %s found (%s) times' . PHP_EOL, $sKey, count($aValue)),
                'light_blue');
            echo '    ';
            $aLines = array();
            foreach ($this->arrayGroupBy($aValue) as $sType => $iCount) {
                $aLines[] = sprintf('%s (%d)', $sType, $iCount);
            }
            echo ColorCLI::getColoredString(implode(', ', $aLines), 'light_gray');
            echo PHP_EOL;
        }

        private function startsWith($aHaystack, $sNeedle)
        {
            // search backwards starting from haystack length characters from the end
//			var_dump('---------------');
//			var_dump($aHaystack);
//			var_dump($sNeedle);
//			var_dump(strrpos($aHaystack, $sNeedle, -strlen($aHaystack)));
            return $sNeedle === "" || strrpos($aHaystack, $sNeedle, -strlen($aHaystack)) !== false;
        }

        private function arrayGroupBy($arr)
        {
            $result = array();
            foreach ($arr as $sKey => $aValue) {
                list($sType, $sFile, $iLine) = $aValue;
                if (isset($result[$sType])) {
                    $result[$sType]++;
                } else {
                    $result[$sType] = self::SKIP_DUE_TO_FILE;
                }
            }
            return $result;
        }

        public function jobReport($sSource, $aDeny, $aAllow, $aSkip, $sDescription = null)
        {

            if ($sDescription !== null) {
                $sDescription = PHP_EOL . PHP_EOL . ' ' . $sDescription . PHP_EOL;
            }

            $sOutput = PHP_EOL . PHP_EOL . ' Starting reek sniffer for the following task ' . PHP_EOL;
            $sOutput .= ' Path to source: ' . $sSource . PHP_EOL;
            $sOutput .= '  Deny:' . PHP_EOL;
            $sOutput .= '%s';
            $sOutput .= '  Allow:' . PHP_EOL;
            $sOutput .= '%s';
            $sOutput .= '  Skip:' . PHP_EOL;
            $sOutput .= '%s';

            if ($sDescription !== null) {
                echo ColorCLI::getColoredString($sDescription, 'black', 'yellow') . PHP_EOL;
            }

            echo ColorCLI::getColoredString(
                    sprintf(
                        $sOutput,
                        $this->displayNamespaces($aDeny),
                        $this->displayNamespaces($aAllow),
                        $this->displayNamespaces($aSkip, '-')
                    ), 'white', 'green') . PHP_EOL;
        }

        private function displayNamespaces($aSet, $sPrefix = '-')
        {
            $sOutput = '';
            if (empty($aSet)) {
                $sOutput .= '        None' . PHP_EOL;
            } else {
                foreach ($aSet as $sNamespace) {
                    $sOutput .= '       ' . $sPrefix . $sNamespace . PHP_EOL;
                }
            }
            return $sOutput;
        }
    }

}
