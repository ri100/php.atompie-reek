<?php
namespace AtomPie\Cli\Reek\View {

    use AtomPie\Cli\Reek\Domain\ReekReport;
    use AtomPie\Cli\Reek\Domain\JobParamSet;

    class ReportRenderer
    {
        private function renderJsonReport(ReekReport $oReekReport)
        {
            echo json_encode($oReekReport);
            $iExitCode = ($oReekReport->hasConflicts())
                ? 1
                : 0;
            return $iExitCode;
        }

        /**
         * @param $jobParams
         * @param $aFoundFiles
         * @param $aSkippedFiles
         * @param $aConflictsReportByFileName
         * @return int
         */
        private function renderConsoleReport(
            JobParamSet $jobParams,
            $aFoundFiles,
            $aSkippedFiles,
            $aConflictsReportByFileName
        ) {
            $cliReportRender = new CliReportRenderer();
            $cliReportRender->jobReport(
                $jobParams->getSource(),
                $jobParams->getDeny(),
                $jobParams->getAllow(),
                $jobParams->getSkip(),
                $jobParams->getDescription()
            );

            if ($jobParams->getVerbose()) {
                if (!empty($aFoundFiles)) {
                    $cliReportRender->displayIteratedFiles($aFoundFiles, $aSkippedFiles, $aConflictsReportByFileName);
                }
            }

            if (!empty($aConflictsReportByFileName)) {
                if ($jobParams->getFlipReporting()) {
                    $cliReportRender->displayFullReport($aConflictsReportByFileName, $jobParams->getWithNamespace());
                } else {
                    $cliReportRender->displayConstrainReport($aConflictsReportByFileName);
                }
            }
            $iExitCode = 0;
            if (!empty($aConflictsReportByFileName)) {
                $iExitCode = 1;
            }

            return $iExitCode;
        }

        /**
         * @param JobParamSet $jobParams
         * @param ReekReport $reekReport
         * @return int Exit code
         */
        public function renderReport(JobParamSet $jobParams, ReekReport $reekReport)
        {

            $aFoundFiles = $reekReport->getFoundFiles();
            $aSkippedFiles = $reekReport->getSkippedFiles();
            $aConflictsReportByFileName = $reekReport->getConflictsReportByFileName();

            if ($jobParams->getJsonReporting() === true) {
                $iExitCode = $this->renderJsonReport($reekReport);
            } else {
                $iExitCode = $this->renderConsoleReport(
                    $jobParams,
                    $aFoundFiles,
                    $aSkippedFiles,
                    $aConflictsReportByFileName
                );
            }
            return $iExitCode;
        }
    }

}
