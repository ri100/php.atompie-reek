<?php
use AtomPie\Cli\Reek\Infrastructure\YamlConfig;
use AtomPie\Cli\Reek\Infrastructure\ColorCLI;
use AtomPie\Cli\Reek\Domain\ReekReport;
use AtomPie\Cli\Reek\Service\CliParams;
use AtomPie\Cli\Reek\Domain\JobParamSet;
use AtomPie\Cli\Reek\Service\ReekFinder;

$oLoader = require __DIR__ . '/../../../../vendor/autoload.php';

/**
 * @param $jobParams
 * @return ReekReport
 */
function findReekingCode(JobParamSet $jobParams)
{
    $reekFinder = new ReekFinder();
    $reekReport = $reekFinder->find(
        $jobParams->getSource(),
        $jobParams->getAllow(),
        $jobParams->getDeny(),
        $jobParams->getSkip(),
        $jobParams->getRecursionDepth()
    );
    return $reekReport;
}

try {

    $iExitCode = 0;

    $cliParams = new CliParams();
    $options = $cliParams->getParamDefinitions();
    $options->parse();

    if ($options->count() == 0) {
        echo "Version: " . CliParams::VERSION . PHP_EOL;
        echo $options->getHelpText();
        exit;
    }

    if (isset($options['version'])) {
        echo "Version: " . CliParams::VERSION . PHP_EOL;
        exit;
    }

    if ($cliParams->hasConfig($options)) {

        $sConfigPath = $cliParams->getConfig($options);
        $sConfigPath = getcwd() . DIRECTORY_SEPARATOR . $sConfigPath;

        $yamlConfig = new YamlConfig($sConfigPath);
        $aYamlConfig = $yamlConfig->parseYaml();

        foreach ($aYamlConfig as $sSource => $aYamlJobConfig) {

            if (substr($sSource, 0, 1) != DIRECTORY_SEPARATOR) {
                $sCurrentDirPath = getcwd() . DIRECTORY_SEPARATOR . $sSource;
            } else {
                $sCurrentDirPath = $sSource;
            }
            
            $sSource = realpath($sCurrentDirPath);

            try {

                $aConfig = $yamlConfig->rewriteYamlConfig($sSource, $aYamlJobConfig);

            } catch (\Exception $e) {

                echo ColorCLI::getColoredString(' ' . $e->getMessage() . ' ', 'white', 'red') . PHP_EOL;
                continue;

            }

            $jobParams = $cliParams->getParamsFromArray($aConfig);
            $reekReport = findReekingCode($jobParams);

            $iExitCode = (new \AtomPie\Cli\Reek\View\ReportRenderer)
                ->renderReport(
                    $jobParams,
                    $reekReport
                );

        }

    } else {

        $jobParams = $cliParams->getParamsFromArray($options);
        $reekReport = findReekingCode($jobParams);

        $iExitCode = (new \AtomPie\Cli\Reek\View\ReportRenderer)
            ->renderReport(
                $jobParams,
                $reekReport
            );

    }

    exit($iExitCode);

} catch (\UnexpectedValueException $e) {
    echo "Error: " . $e->getMessage() . PHP_EOL;
    if (isset($options)) {
        echo PHP_EOL . $options->getHelpText();
    }
} catch (\Exception $e) {
    echo ColorCLI::getColoredString(' ' . $e->getMessage() . ' ', 'white', 'red') . PHP_EOL;
}

exit(1);
