<?php
namespace ReekTestSuit;

use AtomPie\Cli\Reek\Service\ReekFinder;

class ReekFinderTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @test
	 */
	public function shouldFindClassAAsReekSpot() {
		$oReekFinder = new ReekFinder();
		$oReekReport = $oReekFinder->find(
			__DIR__.'/../Resource/src/Namespace1',
			[],
			[ // Deny
				'Namespace2'
			],
			[]
		);
		print_r($oReekReport->getConflictsReportByFileName());
	}
}
