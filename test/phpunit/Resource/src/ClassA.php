<?php
namespace {

	use Namespace1\ClassB;
	use Namespace2\ClassC;

	class ClassA {

		public function x(ClassC $a, ClassB $c) {}

	}

}
