<?php
namespace Namespace1 {

	use Namespace2\ClassC;

	class ClassB {
		public function x(\ClassA $a, ClassC $c) {}
	}

}
