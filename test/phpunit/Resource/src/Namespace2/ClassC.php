<?php
namespace Namespace2 {

	use Namespace1\ClassB;
	use ClassA;

	class ClassC {

		// Must add line to chane line number

		public function x(ClassA $a, ClassB $c) {}

	}

}
